package com.example.reservation.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tables")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableEntity {

    @Id
    @Column(name = "number")
    private int number;

    @Column(name = "seats")
    private int seats;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TableEntity tableEntity = (TableEntity) obj;
        return number == tableEntity.getNumber();
    }

    @Override
    public int hashCode() {
        return 5543;
    }
}
