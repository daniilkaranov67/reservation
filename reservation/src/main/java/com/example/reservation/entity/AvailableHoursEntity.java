package com.example.reservation.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "available_hours")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AvailableHoursEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private int startHour;
    private int endHour;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AvailableHoursEntity availableHoursEntity = (AvailableHoursEntity) obj;
        return id != null && id.equals(availableHoursEntity.getId());
    }

    @Override
    public int hashCode() {
        return 5543;
    }

}
