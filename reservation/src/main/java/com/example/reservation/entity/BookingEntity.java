package com.example.reservation.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "booking", uniqueConstraints = @UniqueConstraint(columnNames = {"booking_date", "table_number", "available_hour_id"}))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "booking_date")
    private LocalDate date;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "table_number")
    private TableEntity tableEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "available_hour_id")
    private AvailableHoursEntity availableHoursEntity;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BookingEntity bookingEntity = (BookingEntity) obj;
        return id != null && id.equals(bookingEntity.getId());
    }

    @Override
    public int hashCode() {
        return 5543;
    }

}
