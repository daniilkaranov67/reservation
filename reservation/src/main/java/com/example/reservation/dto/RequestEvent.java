package com.example.reservation.dto;

public enum RequestEvent {
    APPROVE,
    DISAPPROVE
}
