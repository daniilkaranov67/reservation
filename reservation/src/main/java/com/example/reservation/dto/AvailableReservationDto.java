package com.example.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvailableReservationDto {
    private Integer startHour;
    private Integer endHour;
    private Integer tableNumber;
    private Integer availableHourId;
}
