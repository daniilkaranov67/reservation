package com.example.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateRequestDto {
    private String phone;
    private String date;
    private Integer tableNumber;
    private Integer availableHoursId;
}
