package com.example.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestDto {
    private Integer id;
    private LocalDate date;

    private Integer startHour;
    private Integer endHour;
    private Integer tableNumber;
    private String phone;
}
