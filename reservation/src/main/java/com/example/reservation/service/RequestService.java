package com.example.reservation.service;

import com.example.reservation.dto.BookingDto;
import com.example.reservation.dto.RequestDto;
import com.example.reservation.entity.AvailableHoursEntity;
import com.example.reservation.entity.BookingEntity;
import com.example.reservation.entity.RequestEntity;
import com.example.reservation.repository.BookingRepository;
import com.example.reservation.repository.RequestRepository;
import com.example.reservation.service.exception.BookingAlreadyExists;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RequestService {

    private final RequestRepository requestRepository;
    private final BookingRepository bookingRepository;
    private final ConverterService converterService;

    public RequestService(RequestRepository requestRepository, BookingRepository bookingRepository, ConverterService converterService) {
        this.requestRepository = requestRepository;
        this.bookingRepository = bookingRepository;
        this.converterService = converterService;
    }

    public List<RequestDto> getAll() {
        return requestRepository.findAll().stream().map(converterService::convertRequestToRequestDto).toList();
    }

    public BookingDto approve(Integer id) {
        RequestEntity requestEntity = requestRepository.findById(id).
                orElseThrow(() -> new RuntimeException(String.format("Cannot find request with id %s", id)));
        AvailableHoursEntity availableHoursEntity = requestEntity.getAvailableHoursEntity();
        Optional<BookingEntity> maybeBookingEntity = bookingRepository.findOneByDateAndAvailableHoursEntity(requestEntity.getDate(), availableHoursEntity);
        if (maybeBookingEntity.isEmpty()) {
            BookingEntity bookingEntity = bookingRepository.save(converterService.convertRequestEntityToBookingEntity(requestEntity));
            requestRepository.deleteById(id);
            return converterService.convertBookingToBookingDto(bookingEntity);
        } else {
            throw new BookingAlreadyExists(String.format("Booking on same date already exists. date: %s, startHour: %s, endHour: %s",
                    requestEntity.getDate(), availableHoursEntity.getStartHour(), availableHoursEntity.getEndHour()));
        }
    }

    public void disapprove(Integer id) {
        requestRepository.deleteById(id);
    }
}
