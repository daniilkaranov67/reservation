package com.example.reservation.service;

import com.example.reservation.dto.BookingDto;
import com.example.reservation.dto.RequestDto;
import com.example.reservation.entity.AvailableHoursEntity;
import com.example.reservation.entity.BookingEntity;
import com.example.reservation.entity.RequestEntity;
import org.springframework.stereotype.Service;

@Service
public class ConverterService {
    public RequestDto convertRequestToRequestDto(RequestEntity requestEntity) {
        int tableNum = requestEntity.getTableEntity().getNumber();
        AvailableHoursEntity availableHoursEntity = requestEntity.getAvailableHoursEntity();
        return new RequestDto(
                requestEntity.getId(),
                requestEntity.getDate(),
                availableHoursEntity.getStartHour(),
                availableHoursEntity.getEndHour(),
                tableNum,
                requestEntity.getPhone()
        );
    }

    public BookingEntity convertRequestEntityToBookingEntity(RequestEntity requestEntity) {
        return new BookingEntity(
                requestEntity.getId(),
                requestEntity.getDate(),
                requestEntity.getPhone(),
                requestEntity.getTableEntity(),
                requestEntity.getAvailableHoursEntity()
        );
    }

    public BookingDto convertBookingToBookingDto(BookingEntity bookingEntity) {
        int number = bookingEntity.getTableEntity().getNumber();
        AvailableHoursEntity availableHoursEntity = bookingEntity.getAvailableHoursEntity();
        return  new BookingDto(
                bookingEntity.getId(),
                bookingEntity.getDate(),
                availableHoursEntity.getStartHour(),
                availableHoursEntity.getEndHour(),
                number,
                bookingEntity.getPhone()
        );
    }
}
