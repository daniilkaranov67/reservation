package com.example.reservation.service;

import com.example.reservation.dto.BookingDto;
import com.example.reservation.repository.BookingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final ConverterService converterService;

    public BookingService(BookingRepository bookingRepository, ConverterService converterService) {
        this.bookingRepository = bookingRepository;
        this.converterService = converterService;
    }

    public List<BookingDto> getAll() {
        return bookingRepository.findAll().stream().map(converterService::convertBookingToBookingDto).toList();
    }

    public void deleteById(Integer id) {
        bookingRepository.deleteById(id);
    }
}
