package com.example.reservation.service;

import com.example.reservation.dto.AvailableReservationDto;
import com.example.reservation.dto.RequestDto;
import com.example.reservation.entity.AvailableHoursEntity;
import com.example.reservation.entity.BookingEntity;
import com.example.reservation.entity.RequestEntity;
import com.example.reservation.entity.TableEntity;
import com.example.reservation.repository.AvailableHoursRepository;
import com.example.reservation.repository.BookingRepository;
import com.example.reservation.repository.RequestRepository;
import com.example.reservation.repository.TableRepository;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final BookingRepository bookingRepository;
    private final TableRepository tableRepository;
    private final AvailableHoursRepository availableHoursRepository;
    private final RequestRepository requestRepository;
    private final SimpMessagingTemplate messagingTemplate;
    private final  ConverterService converterService;


    public UserService(BookingRepository bookingRepository, TableRepository tableRepository, AvailableHoursRepository availableHoursRepository, RequestRepository requestRepository, SimpMessagingTemplate messagingTemplate, ConverterService converterService) {
        this.bookingRepository = bookingRepository;
        this.tableRepository = tableRepository;
        this.availableHoursRepository = availableHoursRepository;
        this.requestRepository = requestRepository;
        this.messagingTemplate = messagingTemplate;
        this.converterService = converterService;
    }

    public List<AvailableReservationDto> getAvailableReservation(LocalDate date, Integer seats) {
        List<AvailableHoursEntity> availableHours = availableHoursRepository.findAll();
        List<TableEntity> availableTables = tableRepository.findAllBySeats(seats);
        List<BookingEntity> bookings = bookingRepository.findAllByTableEntityInAndDate(availableTables, date);
        List<AvailableReservationDto> reservation = new ArrayList<>();
        for (AvailableHoursEntity availableHour : availableHours) {
            List<Integer> bookingTablesNumbers = bookings.stream().filter(bookingEntity -> bookingEntity.getAvailableHoursEntity().equals(availableHour)).map(bookingEntity -> bookingEntity.getTableEntity().getNumber()).toList();
            Optional<Integer> freeTableNumber = availableTables.stream().map(TableEntity::getNumber).filter(number -> !bookingTablesNumbers.contains(number)).findAny();
            if (freeTableNumber.isPresent()) {
                reservation.add(new AvailableReservationDto(availableHour.getStartHour(), availableHour.getEndHour(), freeTableNumber.get(), availableHour.getId()));
            }

        }
        return reservation;
    }

    public void createRequest(LocalDate date, Integer tableNumber, Integer availableHourId, String phone) {
        TableEntity tableEntity = tableRepository.findById(tableNumber).orElseThrow(() -> new RuntimeException("Cannot find table with id " + tableNumber));
        AvailableHoursEntity availableHoursEntity = availableHoursRepository.findById(availableHourId).orElseThrow(() -> new RuntimeException("Cannot find availableHours with id" + availableHourId));
        RequestEntity save = requestRepository.save(new RequestEntity(null, date, phone, tableEntity, availableHoursEntity));
        RequestDto requestDto = converterService.convertRequestToRequestDto(save);
        messagingTemplate.convertAndSend("/topic/request", requestDto);
    }
}
