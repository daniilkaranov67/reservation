package com.example.reservation.service.exception;

public class BookingAlreadyExists extends RuntimeException{
    public BookingAlreadyExists(String message) {
        super(message);
    }
}
