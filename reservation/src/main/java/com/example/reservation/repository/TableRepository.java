package com.example.reservation.repository;

import com.example.reservation.entity.TableEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableRepository extends JpaRepository<TableEntity, Integer> {

    List<TableEntity> findAllBySeats(Integer seats);
}
