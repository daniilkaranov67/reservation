package com.example.reservation.repository;

import com.example.reservation.entity.AvailableHoursEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvailableHoursRepository extends JpaRepository<AvailableHoursEntity, Integer> {
}
