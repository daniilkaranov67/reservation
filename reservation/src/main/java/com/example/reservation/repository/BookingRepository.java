package com.example.reservation.repository;

import com.example.reservation.entity.AvailableHoursEntity;
import com.example.reservation.entity.BookingEntity;
import com.example.reservation.entity.TableEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {

    Optional<BookingEntity> findOneByDateAndAvailableHoursEntity(LocalDate date, AvailableHoursEntity availableHoursEntity);

    List<BookingEntity> findAllByTableEntityInAndDate(List<TableEntity> tableEntities, LocalDate date);
}
