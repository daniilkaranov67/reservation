package com.example.reservation.controller.rest;

import com.example.reservation.dto.CreateRequestDto;
import com.example.reservation.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserController {
    //@RequestParam String date, @RequestParam Integer seats

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/available-reservation")
    public ResponseEntity<?> getAvailableReservation(@RequestParam String date, @RequestParam Integer seats) {
        try {
            LocalDate localDate = LocalDate.parse(date);
            return ResponseEntity.ok(userService.getAvailableReservation(localDate, seats));
        } catch (DateTimeParseException e) {
            log.error("Bad time format", e);
            return ResponseEntity.badRequest().body("Bad time format");
        }
    }

    @PostMapping("/request")
    public void createRequest(@RequestBody CreateRequestDto createRequestDto) {
        userService.createRequest(LocalDate.parse(createRequestDto.getDate()), createRequestDto.getTableNumber(), createRequestDto.getAvailableHoursId(), createRequestDto.getPhone());
    }
}
