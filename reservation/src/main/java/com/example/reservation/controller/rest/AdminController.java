package com.example.reservation.controller.rest;

import com.example.reservation.dto.BookingDto;
import com.example.reservation.dto.RequestDto;
import com.example.reservation.dto.RequestEvent;
import com.example.reservation.service.BookingService;
import com.example.reservation.service.RequestService;
import com.example.reservation.service.exception.BookingAlreadyExists;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final BookingService bookingService;
    private final RequestService requestService;

    public AdminController(BookingService bookingService, RequestService requestService) {
        this.bookingService = bookingService;
        this.requestService = requestService;
    }

    @GetMapping("/booking")
    public List<BookingDto> getAllBookings() {
        return bookingService.getAll();
    }

    @GetMapping("/request")
    public List<RequestDto> getAllRequests() {
        return requestService.getAll();
    }

    @PutMapping("/request/{id}/{event}")
    public ResponseEntity<?> changeRequest(@PathVariable Integer id, @PathVariable RequestEvent event) {
        if (event == RequestEvent.APPROVE) {
            try {
                BookingDto bookingDto = requestService.approve(id);
                return ResponseEntity.ok(bookingDto);
            } catch (Exception e) {
                if (e instanceof BookingAlreadyExists) {
                    return ResponseEntity.status(420).body(e.getMessage());
                }
                return ResponseEntity.internalServerError().build();
            }
        } else if (event == RequestEvent.DISAPPROVE) {
            requestService.disapprove(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/booking/{id}")
    public void getAllBookings(@PathVariable Integer id) {
        bookingService.deleteById(id);
    }
}
